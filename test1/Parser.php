<?php
/**
 * Created by PhpStorm.
 * User: grievous
 * Date: 14.06.16
 * Time: 21:21
 */

namespace Gr1ev0us\TestSeopult\Test1;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;


class Parser
{

    const UA = 'TOP-20';

    const TMP = 'tmp';
    /** @var string Url of a target data text */
    protected $url;

    /** @var Client Client of Guzzle */
    protected $client;

    /** @var  WordsStatistics */
    protected $wordStatistics;

    /** @var  LettersStatistics */
    protected $letterStatictics;

    public function __construct($url = 'https://seopult.ru/uploads/File/war_and_peace.txt')
    {
        $this->url = $url;

        $opts = [
            'http' => [
                'follow_location' => 1,
                'max_redirects'   => 20,
                'timeout'         => 10,
                'user_agent'      => self::UA
            ]
        ];
        $this->prepareProxy($opts, $url);

        if (file_exists(self::TMP)) {
            mkdir(self::TMP);
        }

    }

    /**
     * Add proxy support to context if environment variable was set up
     *
     * @param array $opt context options
     * @param string $url
     */
    protected function prepareProxy(&$opt, $url)
    {
        $scheme = parse_url($url)['scheme'];
        if ($scheme === 'http' && (!empty($_SERVER['HTTP_PROXY']) || !empty($_SERVER['http_proxy']))) {
            $proxy = !empty($_SERVER['http_proxy']) ? $_SERVER['http_proxy'] : $_SERVER['HTTP_PROXY'];
        }
        if ($scheme === 'https' && (!empty($_SERVER['HTTPS_PROXY']) || !empty($_SERVER['https_proxy']))) {
            $proxy = !empty($_SERVER['https_proxy']) ? $_SERVER['https_proxy'] : $_SERVER['HTTPS_PROXY'];
        }
        if (!empty($proxy)) {
            $proxy = str_replace(['http://', 'https://'], ['tcp://', 'ssl://'], $proxy);
            $opt['http']['proxy'] = $proxy;
        }
    }

    /**
     * Main method
     */
    public function process()
    {
        foreach ($this->processGenerator() as $action) {
            if (!empty($action)) {
                continue;
            }
            return false;
        }
        return true;
    }

    /**
     * Main generator
     * @return \Generator
     */
    protected function processGenerator()
    {
        if (!file_exists(md5($this->url))) {
            yield $this->download();
        }

        yield $this->parse();

    }

    /**
     * Download method
     * @return bool
     */
    protected function download()
    {
        //todo use streams for big files
        $this->client = new Client();
        $opts = [
            'url'  => $this->url,
            'http' => [
                'follow_location' => 1,
                'max_redirects'   => 20,
                'timeout'         => 10,
                'user_agent'      => self::UA
            ]
        ];
        try {
            $response = $this->client->request('GET');

        } catch (BadResponseException $e) {
            // todo do something with HTTP error
            return false;
        }

        $text = $response->getBody()->getContents();


        file_put_contents(implode(DIRECTORY_SEPARATOR, [self::TMP, md5($this->url)]), $text);


        return true;
    }


    /**
     * Parse method
     */
    protected function parse()
    {
        $resource = fopen(implode(DIRECTORY_SEPARATOR, [self::TMP, md5($this->url)]), 'r');
        try {
            if ($resource) {
                while ($line = fgetc($resource) !== false) {
                    $line = strtolower($line);
                    $this->letterStatictics->processLine($line);
                    $this->wordStatistics->processLine($line);
                }
            }
        } catch (\Exception $e) {
            //todo do something with Read Error
            return false;
        }
        return true;

    }


}